<?php
include 'core.php';
require_once __DIR__.'/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile($config['oauth']['clientSecretPath']);
$client->setRedirectUri($config['oauth']['callbackUrl']);
$client->addScope($config['oauth']['metaData']);

if (! isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    header('Location: ' . filter_var($config['rootUrl'].'/', FILTER_SANITIZE_URL));
}