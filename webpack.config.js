var webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
var staticConfig = require('./webpack.config.phpstorm');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function (env)
{
    var environment = env.environment;

    var postcssLoader = {
        loader: 'postcss-loader',
        options: {
            sourceMap: true,
            plugins: (loader) = [
                require('autoprefixer')({
                    "browsers": [
                        "last 20 version",
                        "Firefox 15"
                    ]
                })
            ]
        }
    };

    var postcssLoaderWithSelectorNamespace = {
        loader: 'postcss-loader',
        options: {
            sourceMap: true,
            plugins: (loader) = [
                require('autoprefixer')({
                    "browsers": [
                        "last 20 version",
                        "Firefox 15"
                    ]
                })
            ]
        }
    };

    var cssLoader = {
        loader: "css-loader", options: {
            sourceMap: true
        }
    };

    var lessLoader = {
        loader: 'less-loader', options: {
            sourceMap: true
        }
    };

    var sassLoader = {
        loader: 'sass-loader', options: {
            sourceMap: true
        }
    };

    if (environment === 'production')
    {
        postcssLoader.options.plugins.push(require('cssnano')());
        postcssLoaderWithSelectorNamespace.options.plugins.push(require('cssnano')());
    }

    var config = {
        watchOptions: {
            poll: 500,
            aggregateTimeout: 300
        },
        devtool: 'source-map',
        context: __dirname,
        entry: staticConfig.entry,
        output: staticConfig.output,
        resolve: staticConfig.resolve,
        externals: staticConfig.externals,
        module: {//Load from end to start
            rules: [
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['env', {
                                targets: {
                                    "chrome": "58",
                                    "ie": "11"
                                }
                            }]
                        ]
                    }
                },
                {
                    test: /\.less$/,
                    exclude: /node_modules/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            cssLoader,
                            postcssLoader,
                            lessLoader
                        ]
                    })
                },
                {
                    test: /\.scss$/,
                    exclude: /node_modules/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            cssLoader,
                            postcssLoaderWithSelectorNamespace,
                            sassLoader
                        ]
                    })
                },
                {
                    test: /\.css$/,
                    exclude: /node_modules/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            cssLoader,
                            postcssLoader
                        ]
                    })
                },
                {
                    test: /no-data-url.(png|gif)$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: 'hashed-files/[name].[hash:6].[ext]'
                            }
                        }
                    ]
                },
                {
                    test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif|cur)(\?\S*)?$/,
                    exclude: [/node_modules/, /no-data-url.(png|gif)$/],
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 8096,//bytes
                                name: 'hashed-files/[name].[hash:6].[ext]',
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(['assets/bundles/*.*', 'assets/bundles/hashed-files']),
            new ExtractTextPlugin('[name].css?hash=[contenthash:6]'),
            new AssetsPlugin(),
            //Allow to use libraries as global variables
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery'
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: 'common',
            }),
        ]
    };
    if (environment === 'production')
    {
        config.devtool = false;
    }

    return config;
};