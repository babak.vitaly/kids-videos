module.exports = {
    entry: {
        pageHome: "./_frontend/pages/home/home.js",
        pageVideo: "./_frontend/pages/video/video.js",
        pageSettings: "./_frontend/pages/settings/settings.js",
    },
    output: {
        path: __dirname + "/assets/bundles",
        filename: "[name].js?hash=[chunkhash:6]",
    },
    resolve: {
        alias: {
            vendor: __dirname + '/_frontend/vendor/',
            modules: __dirname + '/_frontend/modules/',
            utils: __dirname + '/_frontend/utils.js',
            common: __dirname + '/_frontend/common.js',
        }
    },
    externals: {//Use external libraries as local
        // jquery: 'jQuery',
        // $: 'jQuery'
    }
};