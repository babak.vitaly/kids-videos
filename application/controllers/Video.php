<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use YouTube\Models\StreamFormat;
use YouTube\YouTubeDownloader;
use YouTube\Exception\YouTubeException;

class Video extends CI_Controller
{
    public function index($videoId = null)
    {
        redirectBots();
        if ($this->users->getCurrent() === false)
        {
            redirect(base_url('settings/content'));
            die;
        }
        $viewData = array();
        $baseViewData = array(
            'title' => __('video_title'),
            'content' => $this->load->view('video/main', $viewData, true)
        );
        $phpVars =& $this->base->phpVars['pageVideo'];
        $phpVars['templates'] = array(
            'listItem' => $this->load->view('video/listItem', $viewData, true)
        );
        $this->base->addWebpackStyle('pageVideo');
        $this->base->addWebpackScript('pageVideo');
		$this->base->addScript('https://www.youtube.com/iframe_api');
        $this->base->setMainClass('p-0 p-sm-3');
        $this->load->view('base', $baseViewData);
    }

    function play($videoId = null)
    {
        redirectBots();
        $this->index($videoId);
    }

	/**
	 * @param StreamFormat[] $formats
	 * @param int $h
	 * @return StreamFormat
	 */
	private function getFormat($formats, $h = 720)
	{
		$htol = array_values($formats);
		usort($htol, function ($a, $b) {
			/** @var StreamFormat $a */
			/** @var StreamFormat $b */

			return $b->height - $a->height;
		});

		foreach ($htol as $format)
		{
			if (strpos($format->mimeType, 'video/mp4;') === false) continue;
			if (!$format->audioQuality) continue;//Skip formats without an audio

			if ($format->height <= $h) return $format;
		}

		$ltoh = array_values($formats);
		usort($ltoh, function ($a, $b) {
			/** @var StreamFormat $a */
			/** @var StreamFormat $b */

			return $a->height - $b->height;
		});
		return $ltoh[0];
	}

    function getVideoSource()
    {
        //http://kids-videos.local/vendor/jeckman/YouTube-Downloader/getvideo.php?videoid=zZx5lIE7M2E&format=720

		$youtube = new YouTubeDownloader();
		$downloadOptions = $youtube->getDownloadLinks("https://www.youtube.com/watch?v={$_GET['videoid']}");

		if ($formats = $downloadOptions->getAllFormats()) {
			$format = $this->getFormat($formats, $_GET['format']);
			//var_dump('$format', $format);
			$video = $format->url;
		} else {
			$video = false;
		}
		redirect($video, 'auto', 302);
    }

    function ajaxGetVideos()
    {
        $data = getAjaxData();
        $this->form_validation->reset_validation();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules(
            'count',
            'count',
            'required|is_natural|min_length[1]|trim'
        );
        if ($this->form_validation->run() === false)
        {
            ajaxReturn(array(
                'state' => false,
                'message' => validation_errors()
            ));
        }
		$GLOBALS['debug'] = '';
        $videos = $this->youtube->getVideos(array(
            'count' => set_value('count'),
        ));

        if ($videos === false)
        {
            ajaxReturn(array(
                'state' => false,
                'message' => __('video_error_cant_get_video'),
				//'debug' => $GLOBALS['debug'],
            ));
        }

        if (sizeof($videos) === 0)
        {
            ajaxReturn(array(
                'state' => false,
                'message' => __('video_notice_there_is_not_video_to_play'),
				//'debug' => $GLOBALS['debug'],
            ));
        }

        ajaxReturn(array(
            'state' => true,
            'videos' => $videos,
			//'debug' => $GLOBALS['debug'],
        ));
    }


    function ajaxGetVideo()
    {
        $data = getAjaxData();
        $this->form_validation->reset_validation();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules(
            'id',
            'id',
            'required|min_length[0]|max_length[255]|trim'
        );
        if ($this->form_validation->run() === false)
        {
            ajaxReturn(array(
                'state' => false,
                'message' => validation_errors()
            ));
        }
        $video = $this->youtube->getVideo(set_value('id'));

        if ($video === false)
        {
            ajaxReturn(array(
                'state' => false,
                'message' => __('video_error_cant_get_video')
            ));
        }

        ajaxReturn(array(
            'state' => true,
            'video' => $video
        ));
    }
}
