<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{
    public function index()
    {
        redirectBots();
        $user = $this->users->getCurrent();

        $viewData = array(
            'general' => $user['settings']['general'],
            'secretUrl' => base_url() . "login/by_url/{$user['login_key']}"
        );
        $tabContent = $this->load->view('settings/tabs/general', $viewData, true);
        $baseViewData = array(
            'title' => __('settings_general_title'),
            'content' => $this->load->view('settings/base', array('content' => $tabContent), true)
        );
        $this->base->addWebpackStyle('pageSettings');
        $this->base->addWebpackScript('pageSettings');
        $this->base->setPageName('settingsGeneral');
        $this->load->view('base', $baseViewData);
    }

    public function content()
    {
        redirectBots();
        $viewData = array();
        $user = $this->users->getCurrent();
        $tabContent = $this->load->view('settings/tabs/content', $viewData, true);
        $baseViewData = array(
            'title' => __('settings_content_title'),
            'content' => $this->load->view('settings/base', array('content' => $tabContent), true)
        );

        foreach ($user['settings']['content']['channels'] as $key => &$userChannel)
        {
            $channel = $this->youtube->getChannelById($userChannel['id']);
            if (!isset($channel['items'][0]))
            {
                unset($user['settings']['content']['channels'][$key]);
                continue;
            }
            $channelItem = $channel['items'][0];
            $userChannel['title'] = $channelItem['snippet']['title'];
            $userChannel['settings']['include'] = htmlspecialchars_decode($userChannel['settings']['include']);
        }
        $this->base->phpVars['pageSettingsContent'] = array(
            'templates' => array(
                'channel' => $this->load->view('settings/tabs/contentChannel', array(), true)
            ),
            'settings' => $user['settings']['content']
        );
        $this->base->addWebpackStyle('pageSettings');
        $this->base->addWebpackScript('pageSettings');
        $this->base->setPageName('settingsContent');
        $this->load->view('base', $baseViewData);
    }

    public function ajaxGeneralSave()
    {
        $this->form_validation->reset_validation();
        $this->form_validation->set_data(getAjaxData());

//        $this->form_validation->set_rules(
//            'password',
//            'Пароль',
//            'min_length[0]|max_length[255]|trim'
//        );
//        $this->form_validation->set_message('url', 'qwe');

        if ($this->form_validation->run() === false)
        {
            ajaxReturn(array('state' => false, 'message' => validation_errors()));
        }

        $user = $this->users->getCurrent();
        $user['settings']['general'] = array(
            'password' => set_value('password')
        );
        $this->users->update($user);
        ajaxReturn(array('state' => true));
    }

    public function ajaxContentSave()
    {
        $input = getAjaxData();

        foreach ($input['channels'] as $key => &$channel)
        {
            $this->form_validation->reset_validation();
            $this->form_validation->set_data($channel);
            $this->form_validation->set_rules(
                'id',
                'Id',
                'required|min_length[1]|max_length[255]|trim'
            );
            $this->form_validation->set_rules(
                'settings[state]',
                __('settings_content_channels_checkbox_state'),
                'min_length[0]|max_length[255]|trim'
            );
            $this->form_validation->set_rules(
                'settings[include]',
                __('settings_content_channels_field_search_label'),
                'min_length[0]|max_length[255]|trim'
            );
            $this->form_validation->set_rules(
                'settings[priority]',
                __('settings_content_channels_field_priority_label'),
                'required|is_natural|min_length[1]|trim'
            );
            if ($this->form_validation->run() === false)
            {
                ajaxReturn(array(
                    'state' => false,
                    'message' => validation_errors(),
                    'section' => 'channels',
                    'fieldNumber' => $key
                ));
            }
            $channel = array(
                'id' => set_value('id'),
                'settings' => array(
                    'state' => set_value('settings[state]'),
                    'include' => set_value('settings[include]'),
                    'priority' => set_value('settings[priority]'),
                )
            );
        }
        $user = $this->users->getCurrent();
        $user['settings']['content'] = array(
            'channels' => $input['channels']
        );
        $this->users->update($user);
        ajaxReturn(array('state' => true));
    }

    public function ajaxContentAddChannel()
    {
        $input = getAjaxData();
        //https://www.youtube.com/user/TeremokTV
        //https://www.youtube.com/channel/UCVEDZVtA5NUtjxSXHjtvkag

        $this->form_validation->reset_validation();
        $this->form_validation->set_data($input);

        $this->form_validation->set_rules(
            'url',
            '',
          	'required'//  'required|regex_match[/^(https:\/\/www\.youtube\.com\/(channel\/|user\/).*$/]'
        );
        $this->form_validation->set_message('url', __('settings_content_channels_field_channel_link_validation_incorrect'));

        if ($this->form_validation->run() === false)
        {
            ajaxReturn(array('state' => false, 'message' => validation_errors()));
        }

        $channel = $this->youtube->getChannelByUrl(set_value('url'));
        if (!isset($channel['items'][0]))
        {
            ajaxReturn(array(
                'state' => false,
                'message' => __('settings_content_channels_field_channel_link_validation_not_found')
            ));
        }
        $channelItem = $channel['items'][0];
        ajaxReturn(array(
            'state' => true,
            'channel' => array(
                'id' => $channelItem['id'],
                'title' => $channelItem['snippet']['title'],
                'settings' => array('state' => 'on')
            )
        ));
    }
}
