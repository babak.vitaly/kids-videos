<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller
{
    public function index()
    {
        $baseViewData = array(
            'title' => __('home_title'),
            'content' => $this->load->view(getLangCode() === 'en' ? 'home' : 'home_ru', array(), true)
        );
        $this->base->addWebpackStyle('pageHome');
        $this->base->addWebpackScript('pageHome');
        $this->base->setPageName('home');
        $this->load->view('base', $baseViewData);
    }
}
