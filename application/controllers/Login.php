<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function by_url($key)
    {
        redirectBots();
        $this->users->loginByKey($key);
        redirect(base_url('video'));
    }
}
