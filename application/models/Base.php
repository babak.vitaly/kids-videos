<?php

class Base extends CI_Model
{
    private $styles = array();
    private $scripts = array();
    private $webpackAssets, $pageName = '', $mainClass = '';
    public $phpVars = array();

    function __construct()
    {
        $str = file_get_contents(FCPATH . 'webpack-assets.json');
        $this->webpackAssets = json_decode($str, true);
        $this->addWebpackScript('common');
        $this->addWebpackStyle('common');
        $this->phpVars = array(
            'baseUrl' => base_url(),
            'strings' => $this->lang->language,
        );
    }

    function setMainClass($mainClass)
    {
        $this->mainClass = $mainClass;
    }

    function getMainClass()
    {
        return $this->mainClass;
    }

    function getPageName()
    {
        return $this->pageName;
    }

    function setPageName($pageName)
    {
        $this->pageName = $pageName;
    }

    function addScript($name)
    {
        $this->scripts[] = $name;
    }

    function addStyle($name)
    {
        $this->styles[] = $name;
    }

    function addWebpackScript($name)
    {
        if (!isset($this->webpackAssets[$name]['js'])) return;
        $this->scripts[] = base_url("assets/bundles/{$this->webpackAssets[$name]['js']}");
    }

    function addWebpackStyle($name)
    {
        if (!isset($this->webpackAssets[$name]['css'])) return;
        $this->styles[] = base_url("assets/bundles/{$this->webpackAssets[$name]['css']}");
    }

    function printStyles()
    {
        foreach ($this->styles as $style)
        {
            echo "<link href='{$style}' rel='stylesheet'>\r\n";
        }
    }

    function printScripts()
    {
        foreach ($this->scripts as $script)
        {
            echo "<script src='{$script}'></script>\r\n";
        }
    }

    function printPhpVars()
    {
        echo "<script>window.phpVars = " . json_encode($this->phpVars) . ";</script>";
    }
}