<?php
include_once(FCPATH . "/application/libraries/Cache.php");

/**
 * @property Cache $cache
 */
class Youtube extends CI_Model
{
    public $service;

    function __construct()
    {
        $client = new Google_Client();
        $client->setDeveloperKey('AIzaSyAY1Sb7VFQm-k1g2qLubnQdZGs4948H9NU');
        $this->service = new Google_Service_YouTube($client);
    }

	function getChannelByUrl($url)
	{
		if (strpos($url, 'https://') !== false)
		{
			$re = '/youtube\.com\/(@|channel|user)\/?(.*)$/';
			preg_match($re, $url, $matches);

			if (empty($matches[1]))
			{
				throw new \Exception('empty($matches[1])');
			}
			if (empty($matches[2]))
			{
				throw new \Exception('empty($matches[2])');
			}
			$type = $matches[1];
			$id = $matches[2];

			if ($type === '@')
			{
				$channel = $this->getChannelByHandle("@$id");
				if (isset($channel['items'][0]['id']['channelId']))
				{
					$channel['items'][0]['id'] = $channel['items'][0]['id']['channelId'];
				}
				return $channel;
			} elseif ($type === 'user') return $this->getChannelByName($id);
			elseif ($type === 'channel') return $this->getChannelById($id);
			else throw new \Exception('Wrong type.');
		} else
		{
			return $this->getChannelById($url);
		}
	}

    /**
     * @param callback $call
     * @param array $params
     * @return array
     */
    function getData($call, $params)
    {
        $defaults = array(
            'part' => 'id,snippet'
        );
        $params = array_replace_recursive($defaults, $params);
        $cacheParams = array(
            'parameters' => $params,
            'directory' => get_class($call[0]),
			'callbackGetNewData' => function ($params) use ($call) {
				$response = call_user_func_array($call, array($params['part'], $params));
				return array(
					'state' => true,
					'data' => objectToArray($response->toSimpleObject())
				);
			}
        );
        $cache = new Cache($cacheParams);
        $data = $cache->getData();
        return $data;
    }

    function getChannelById($id)
    {
        $response = $this->youtube->getData(
            array($this->service->channels, 'listChannels'),
            array('id' => $id)
        );
        if ($response['state'] === false) return false;
        return $response['data'];
    }

    function getChannelByName($name)
    {
        $response = $this->youtube->getData(
            array($this->service->channels, 'listChannels'),
            array('forUsername' => $name)
        );

        if ($response['state'] === false) return false;
        return $response['data'];
    }

	function getChannelByHandle($handle)
	{
		$response = $this->youtube->getData(
			array($this->service->search, 'listSearch'),
			array(
				'part' => 'snippet',
				'q' => $handle,
				'type' => 'channel',
				'maxResults' => 1
			)
		);

		if ($response['state'] === false) return false;
		return $response['data'];
	}

    function getVideo($id)
    {
        $response = $this->youtube->getData(
            array($this->service->videos, 'listVideos'),
            array('id' => $id)
        );
        if ($response['state'] === false || !isset($response['data']['items'][0])) return false;
        return $response['data']['items'][0];
    }

    function getRandomVideosFromChannel($channel, $count)
    {
        $videos = array();
		$maxResultsPerPage = 50;

		//Max is 500, https://developers.google.com/youtube/v3/docs/search/list#type
		$maxTotalPages = 500 / $maxResultsPerPage;
        $searchArgs = array(
			'type' => 'video',
            'channelId' => $channel['id'],
            'part' => 'snippet,id',            
            'maxResults' => $maxResultsPerPage,
            'q' => '',
			'videoEmbeddable' => '1',
			'order' => array_rand(['date', 'rating', 'relevance', 'title', 'viewCount']),			
			/*
			 * any – Do not filter video search results based on their duration. This is the default value.
long – Only include videos longer than 20 minutes.
medium – Only include videos that are between four and 20 minutes long (inclusive).
short – Only include videos that are less than four minutes long.
			 */
			//'videoDuration' => array_rand(['any', 'long', 'medium', 'short']),
        );		
        if ($channel['settings']['include'] !== '')
        {
            $searchArgs['q'] = htmlspecialchars_decode($channel['settings']['include']);
        }
        $basePage = $this->youtube->getData(
            array($this->service->search, 'listSearch'),
            $searchArgs
        );
        $basePage = $basePage['data'];
        $pages = ceil($basePage['pageInfo']['totalResults'] / $maxResultsPerPage);
        if ($pages > $maxTotalPages) $pages = $maxTotalPages;
        if ($count > $basePage['pageInfo']['totalResults'])
        {
            $count = $basePage['pageInfo']['totalResults'];
        }
        $c1 = 2;
		$processedPages = [];
		$GLOBALS['debug'] .= '//$basePage' . "\n" . var_export($basePage, true) . ";\n";
		
        while ($c1)
        {
            $page = rand(1, $pages);			
			if(!in_array($page, $processedPages))
			{
				$pageToken = $this->getPageToken($page);

				$listSearch = $this->youtube->getData(
					array($this->service->search, 'listSearch'),
					$searchArgs + array('pageToken' => $pageToken)
				);
				$GLOBALS['debug'] .= '//$page = '. var_export($page, true).";\n";
				$GLOBALS['debug'] .= '//$pageToken = '. var_export($pageToken, true).";\n";
				$GLOBALS['debug'] .= '//$searchArgs = '. var_export($searchArgs + array('pageToken' => $pageToken), true).";\n";
				$GLOBALS['debug'] .= '//$listSearch = '. var_export($listSearch, true).";\n";
				
				$listSearch = $listSearch['data'];
				
				$videos = array_merge($videos, $listSearch['items']);
				$processedPages[] = $page;
				if (sizeof($videos) >= $count) goto end;
			}
            $c1--;
        }

        end:
		shuffle($videos);
        return array_slice($videos, 0, $count);
    }

    function getPageToken($page)
    {
        $tokens = array(
            'CAAQAA', 'CAEQAA', 'CAIQAA', 'CAMQAA', 'CAQQAA', 'CAUQAA', 'CAYQAA', 'CAcQAA', 'CAgQAA', 'CAkQAA', 'CAoQAA', 'CAsQAA', 'CAwQAA', 'CA0QAA', 'CA4QAA', 'CA8QAA', 'CBAQAA', 'CBEQAA', 'CBIQAA', 'CBMQAA', 'CBQQAA', 'CBUQAA', 'CBYQAA', 'CBcQAA', 'CBgQAA', 'CBkQAA', 'CBoQAA', 'CBsQAA', 'CBwQAA', 'CB0QAA', 'CB4QAA', 'CB8QAA', 'CCAQAA', 'CCEQAA', 'CCIQAA', 'CCMQAA', 'CCQQAA', 'CCUQAA', 'CCYQAA', 'CCcQAA', 'CCgQAA', 'CCkQAA', 'CCoQAA', 'CCsQAA', 'CCwQAA', 'CC0QAA', 'CC4QAA', 'CC8QAA', 'CDAQAA', 'CDEQAA', 'CDIQAA', 'CDMQAA', 'CDQQAA', 'CDUQAA', 'CDYQAA', 'CDcQAA', 'CDgQAA', 'CDkQAA', 'CDoQAA', 'CDsQAA', 'CDwQAA', 'CD0QAA', 'CD4QAA', 'CD8QAA', 'CEAQAA', 'CEEQAA', 'CEIQAA', 'CEMQAA', 'CEQQAA', 'CEUQAA', 'CEYQAA', 'CEcQAA', 'CEgQAA', 'CEkQAA', 'CEoQAA', 'CEsQAA', 'CEwQAA', 'CE0QAA', 'CE4QAA', 'CE8QAA', 'CFAQAA', 'CFEQAA', 'CFIQAA', 'CFMQAA', 'CFQQAA', 'CFUQAA', 'CFYQAA', 'CFcQAA', 'CFgQAA', 'CFkQAA', 'CFoQAA', 'CFsQAA', 'CFwQAA', 'CF0QAA', 'CF4QAA', 'CF8QAA', 'CGAQAA', 'CGEQAA', 'CGIQAA', 'CGMQAA', 'CGQQAA', 'CGUQAA', 'CGYQAA', 'CGcQAA', 'CGgQAA', 'CGkQAA', 'CGoQAA', 'CGsQAA', 'CGwQAA', 'CG0QAA', 'CG4QAA', 'CG8QAA', 'CHAQAA', 'CHEQAA', 'CHIQAA', 'CHMQAA', 'CHQQAA', 'CHUQAA', 'CHYQAA', 'CHcQAA', 'CHgQAA', 'CHkQAA', 'CHoQAA', 'CHsQAA', 'CHwQAA', 'CH0QAA', 'CH4QAA', 'CH8QAA', 'CIABEAA', 'CIEBEAA', 'CIIBEAA', 'CIMBEAA', 'CIQBEAA', 'CIUBEAA', 'CIYBEAA', 'CIcBEAA', 'CIgBEAA', 'CIkBEAA', 'CIoBEAA', 'CIsBEAA', 'CIwBEAA', 'CI0BEAA', 'CI4BEAA', 'CI8BEAA', 'CJABEAA', 'CJEBEAA', 'CJIBEAA', 'CJMBEAA', 'CJQBEAA', 'CJUBEAA', 'CJYBEAA', 'CJcBEAA', 'CJgBEAA', 'CJkBEAA', 'CJoBEAA', 'CJsBEAA', 'CJwBEAA', 'CJ0BEAA', 'CJ4BEAA', 'CJ8BEAA', 'CKABEAA', 'CKEBEAA', 'CKIBEAA', 'CKMBEAA', 'CKQBEAA', 'CKUBEAA', 'CKYBEAA', 'CKcBEAA', 'CKgBEAA', 'CKkBEAA', 'CKoBEAA', 'CKsBEAA', 'CKwBEAA', 'CK0BEAA', 'CK4BEAA', 'CK8BEAA', 'CLABEAA', 'CLEBEAA', 'CLIBEAA', 'CLMBEAA', 'CLQBEAA', 'CLUBEAA', 'CLYBEAA', 'CLcBEAA', 'CLgBEAA', 'CLkBEAA', 'CLoBEAA', 'CLsBEAA', 'CLwBEAA', 'CL0BEAA', 'CL4BEAA', 'CL8BEAA', 'CMABEAA', 'CMEBEAA', 'CMIBEAA', 'CMMBEAA', 'CMQBEAA', 'CMUBEAA', 'CMYBEAA', 'CMcBEAA', 'CMgBEAA', 'CMkBEAA', 'CMoBEAA', 'CMsBEAA', 'CMwBEAA', 'CM0BEAA', 'CM4BEAA', 'CM8BEAA', 'CNABEAA', 'CNEBEAA', 'CNIBEAA', 'CNMBEAA', 'CNQBEAA', 'CNUBEAA', 'CNYBEAA', 'CNcBEAA', 'CNgBEAA', 'CNkBEAA', 'CNoBEAA', 'CNsBEAA', 'CNwBEAA', 'CN0BEAA', 'CN4BEAA', 'CN8BEAA', 'COABEAA', 'COEBEAA', 'COIBEAA', 'COMBEAA', 'COQBEAA', 'COUBEAA', 'COYBEAA', 'COcBEAA', 'COgBEAA', 'COkBEAA', 'COoBEAA', 'COsBEAA', 'COwBEAA', 'CO0BEAA', 'CO4BEAA', 'CO8BEAA', 'CPABEAA', 'CPEBEAA', 'CPIBEAA', 'CPMBEAA', 'CPQBEAA', 'CPUBEAA', 'CPYBEAA', 'CPcBEAA', 'CPgBEAA', 'CPkBEAA', 'CPoBEAA', 'CPsBEAA', 'CPwBEAA', 'CP0BEAA', 'CP4BEAA', 'CP8BEAA', 'CIACEAA', 'CIECEAA', 'CIICEAA', 'CIMCEAA', 'CIQCEAA', 'CIUCEAA', 'CIYCEAA', 'CIcCEAA', 'CIgCEAA', 'CIkCEAA', 'CIoCEAA', 'CIsCEAA', 'CIwCEAA', 'CI0CEAA', 'CI4CEAA', 'CI8CEAA', 'CJACEAA', 'CJECEAA', 'CJICEAA', 'CJMCEAA', 'CJQCEAA', 'CJUCEAA', 'CJYCEAA', 'CJcCEAA', 'CJgCEAA', 'CJkCEAA', 'CJoCEAA', 'CJsCEAA', 'CJwCEAA', 'CJ0CEAA', 'CJ4CEAA', 'CJ8CEAA', 'CKACEAA', 'CKECEAA', 'CKICEAA', 'CKMCEAA', 'CKQCEAA', 'CKUCEAA', 'CKYCEAA', 'CKcCEAA', 'CKgCEAA', 'CKkCEAA', 'CKoCEAA', 'CKsCEAA', 'CKwCEAA', 'CK0CEAA', 'CK4CEAA', 'CK8CEAA', 'CLACEAA', 'CLECEAA', 'CLICEAA', 'CLMCEAA', 'CLQCEAA', 'CLUCEAA', 'CLYCEAA', 'CLcCEAA', 'CLgCEAA', 'CLkCEAA', 'CLoCEAA', 'CLsCEAA', 'CLwCEAA', 'CL0CEAA', 'CL4CEAA', 'CL8CEAA', 'CMACEAA', 'CMECEAA', 'CMICEAA', 'CMMCEAA', 'CMQCEAA', 'CMUCEAA', 'CMYCEAA', 'CMcCEAA', 'CMgCEAA', 'CMkCEAA', 'CMoCEAA', 'CMsCEAA', 'CMwCEAA', 'CM0CEAA', 'CM4CEAA', 'CM8CEAA', 'CNACEAA', 'CNECEAA', 'CNICEAA', 'CNMCEAA', 'CNQCEAA', 'CNUCEAA', 'CNYCEAA', 'CNcCEAA', 'CNgCEAA', 'CNkCEAA', 'CNoCEAA', 'CNsCEAA', 'CNwCEAA', 'CN0CEAA', 'CN4CEAA', 'CN8CEAA', 'COACEAA', 'COECEAA', 'COICEAA', 'COMCEAA', 'COQCEAA', 'COUCEAA', 'COYCEAA', 'COcCEAA', 'COgCEAA', 'COkCEAA', 'COoCEAA', 'COsCEAA', 'COwCEAA', 'CO0CEAA', 'CO4CEAA', 'CO8CEAA', 'CPACEAA', 'CPECEAA', 'CPICEAA', 'CPMCEAA', 'CPQCEAA', 'CPUCEAA', 'CPYCEAA', 'CPcCEAA', 'CPgCEAA', 'CPkCEAA', 'CPoCEAA', 'CPsCEAA', 'CPwCEAA', 'CP0CEAA', 'CP4CEAA', 'CP8CEAA', 'CIADEAA', 'CIEDEAA', 'CIIDEAA', 'CIMDEAA', 'CIQDEAA', 'CIUDEAA', 'CIYDEAA', 'CIcDEAA', 'CIgDEAA', 'CIkDEAA', 'CIoDEAA', 'CIsDEAA', 'CIwDEAA', 'CI0DEAA', 'CI4DEAA', 'CI8DEAA', 'CJADEAA', 'CJEDEAA', 'CJIDEAA', 'CJMDEAA', 'CJQDEAA', 'CJUDEAA', 'CJYDEAA', 'CJcDEAA', 'CJgDEAA', 'CJkDEAA', 'CJoDEAA', 'CJsDEAA', 'CJwDEAA', 'CJ0DEAA', 'CJ4DEAA', 'CJ8DEAA', 'CKADEAA', 'CKEDEAA', 'CKIDEAA', 'CKMDEAA', 'CKQDEAA', 'CKUDEAA', 'CKYDEAA', 'CKcDEAA', 'CKgDEAA', 'CKkDEAA', 'CKoDEAA', 'CKsDEAA', 'CKwDEAA', 'CK0DEAA', 'CK4DEAA', 'CK8DEAA', 'CLADEAA', 'CLEDEAA', 'CLIDEAA', 'CLMDEAA', 'CLQDEAA', 'CLUDEAA', 'CLYDEAA', 'CLcDEAA', 'CLgDEAA', 'CLkDEAA', 'CLoDEAA', 'CLsDEAA', 'CLwDEAA', 'CL0DEAA', 'CL4DEAA', 'CL8DEAA', 'CMADEAA', 'CMEDEAA', 'CMIDEAA', 'CMMDEAA', 'CMQDEAA', 'CMUDEAA', 'CMYDEAA', 'CMcDEAA', 'CMgDEAA', 'CMkDEAA', 'CMoDEAA', 'CMsDEAA', 'CMwDEAA', 'CM0DEAA', 'CM4DEAA', 'CM8DEAA', 'CNADEAA', 'CNEDEAA', 'CNIDEAA', 'CNMDEAA', 'CNQDEAA', 'CNUDEAA', 'CNYDEAA', 'CNcDEAA', 'CNgDEAA', 'CNkDEAA', 'CNoDEAA', 'CNsDEAA', 'CNwDEAA', 'CN0DEAA', 'CN4DEAA', 'CN8DEAA', 'COADEAA', 'COEDEAA', 'COIDEAA', 'COMDEAA', 'COQDEAA', 'COUDEAA', 'COYDEAA', 'COcDEAA', 'COgDEAA', 'COkDEAA', 'COoDEAA', 'COsDEAA', 'COwDEAA', 'CO0DEAA', 'CO4DEAA', 'CO8DEAA', 'CPADEAA', 'CPEDEAA', 'CPIDEAA', 'CPMDEAA', 'CPQDEAA', 'CPUDEAA', 'CPYDEAA', 'CPcDEAA', 'CPgDEAA', 'CPkDEAA', 'CPoDEAA', 'CPsDEAA', 'CPwDEAA', 'CP0DEAA', 'CP4DEAA', 'CP8DEAA', 'CIAEEAA', 'CIEEEAA', 'CIIEEAA', 'CIMEEAA', 'CIQEEAA', 'CIUEEAA', 'CIYEEAA', 'CIcEEAA', 'CIgEEAA', 'CIkEEAA', 'CIoEEAA', 'CIsEEAA', 'CIwEEAA', 'CI0EEAA', 'CI4EEAA', 'CI8EEAA', 'CJAEEAA', 'CJEEEAA', 'CJIEEAA', 'CJMEEAA', 'CJQEEAA', 'CJUEEAA', 'CJYEEAA', 'CJcEEAA', 'CJgEEAA', 'CJkEEAA', 'CJoEEAA', 'CJsEEAA', 'CJwEEAA', 'CJ0EEAA', 'CJ4EEAA', 'CJ8EEAA', 'CKAEEAA', 'CKEEEAA', 'CKIEEAA', 'CKMEEAA', 'CKQEEAA', 'CKUEEAA', 'CKYEEAA', 'CKcEEAA', 'CKgEEAA', 'CKkEEAA', 'CKoEEAA', 'CKsEEAA', 'CKwEEAA', 'CK0EEAA', 'CK4EEAA', 'CK8EEAA', 'CLAEEAA', 'CLEEEAA', 'CLIEEAA', 'CLMEEAA', 'CLQEEAA', 'CLUEEAA', 'CLYEEAA', 'CLcEEAA', 'CLgEEAA', 'CLkEEAA', 'CLoEEAA', 'CLsEEAA', 'CLwEEAA', 'CL0EEAA', 'CL4EEAA', 'CL8EEAA', 'CMAEEAA', 'CMEEEAA', 'CMIEEAA', 'CMMEEAA', 'CMQEEAA', 'CMUEEAA', 'CMYEEAA', 'CMcEEAA', 'CMgEEAA', 'CMkEEAA', 'CMoEEAA', 'CMsEEAA', 'CMwEEAA', 'CM0EEAA', 'CM4EEAA', 'CM8EEAA', 'CNAEEAA', 'CNEEEAA', 'CNIEEAA', 'CNMEEAA', 'CNQEEAA', 'CNUEEAA', 'CNYEEAA', 'CNcEEAA', 'CNgEEAA', 'CNkEEAA', 'CNoEEAA', 'CNsEEAA', 'CNwEEAA', 'CN0EEAA', 'CN4EEAA', 'CN8EEAA', 'COAEEAA', 'COEEEAA', 'COIEEAA', 'COMEEAA', 'COQEEAA', 'COUEEAA', 'COYEEAA', 'COcEEAA', 'COgEEAA', 'COkEEAA', 'COoEEAA', 'COsEEAA', 'COwEEAA', 'CO0EEAA', 'CO4EEAA', 'CO8EEAA', 'CPAEEAA', 'CPEEEAA', 'CPIEEAA', 'CPMEEAA', 'CPQEEAA', 'CPUEEAA', 'CPYEEAA', 'CPcEEAA', 'CPgEEAA', 'CPkEEAA', 'CPoEEAA', 'CPsEEAA', 'CPwEEAA', 'CP0EEAA', 'CP4EEAA', 'CP8EEAA', 'CIAFEAA', 'CIEFEAA', 'CIIFEAA', 'CIMFEAA', 'CIQFEAA', 'CIUFEAA', 'CIYFEAA', 'CIcFEAA', 'CIgFEAA', 'CIkFEAA', 'CIoFEAA', 'CIsFEAA', 'CIwFEAA', 'CI0FEAA', 'CI4FEAA', 'CI8FEAA', 'CJAFEAA', 'CJEFEAA', 'CJIFEAA', 'CJMFEAA', 'CJQFEAA', 'CJUFEAA', 'CJYFEAA', 'CJcFEAA', 'CJgFEAA', 'CJkFEAA', 'CJoFEAA', 'CJsFEAA', 'CJwFEAA', 'CJ0FEAA', 'CJ4FEAA', 'CJ8FEAA', 'CKAFEAA', 'CKEFEAA', 'CKIFEAA', 'CKMFEAA', 'CKQFEAA', 'CKUFEAA', 'CKYFEAA', 'CKcFEAA', 'CKgFEAA', 'CKkFEAA', 'CKoFEAA', 'CKsFEAA', 'CKwFEAA', 'CK0FEAA', 'CK4FEAA', 'CK8FEAA', 'CLAFEAA', 'CLEFEAA', 'CLIFEAA', 'CLMFEAA', 'CLQFEAA', 'CLUFEAA', 'CLYFEAA', 'CLcFEAA', 'CLgFEAA', 'CLkFEAA', 'CLoFEAA', 'CLsFEAA', 'CLwFEAA', 'CL0FEAA', 'CL4FEAA', 'CL8FEAA', 'CMAFEAA', 'CMEFEAA', 'CMIFEAA', 'CMMFEAA', 'CMQFEAA', 'CMUFEAA', 'CMYFEAA', 'CMcFEAA', 'CMgFEAA', 'CMkFEAA', 'CMoFEAA', 'CMsFEAA', 'CMwFEAA', 'CM0FEAA', 'CM4FEAA', 'CM8FEAA', 'CNAFEAA', 'CNEFEAA', 'CNIFEAA', 'CNMFEAA', 'CNQFEAA', 'CNUFEAA', 'CNYFEAA', 'CNcFEAA', 'CNgFEAA', 'CNkFEAA', 'CNoFEAA', 'CNsFEAA', 'CNwFEAA', 'CN0FEAA', 'CN4FEAA', 'CN8FEAA', 'COAFEAA', 'COEFEAA', 'COIFEAA', 'COMFEAA', 'COQFEAA', 'COUFEAA', 'COYFEAA', 'COcFEAA', 'COgFEAA', 'COkFEAA', 'COoFEAA', 'COsFEAA', 'COwFEAA', 'CO0FEAA', 'CO4FEAA', 'CO8FEAA', 'CPAFEAA', 'CPEFEAA', 'CPIFEAA', 'CPMFEAA', 'CPQFEAA', 'CPUFEAA', 'CPYFEAA', 'CPcFEAA', 'CPgFEAA', 'CPkFEAA', 'CPoFEAA', 'CPsFEAA', 'CPwFEAA', 'CP0FEAA', 'CP4FEAA', 'CP8FEAA', 'CIAGEAA', 'CIEGEAA', 'CIIGEAA', 'CIMGEAA', 'CIQGEAA', 'CIUGEAA', 'CIYGEAA', 'CIcGEAA', 'CIgGEAA', 'CIkGEAA', 'CIoGEAA', 'CIsGEAA', 'CIwGEAA', 'CI0GEAA', 'CI4GEAA', 'CI8GEAA', 'CJAGEAA', 'CJEGEAA', 'CJIGEAA', 'CJMGEAA', 'CJQGEAA', 'CJUGEAA', 'CJYGEAA', 'CJcGEAA', 'CJgGEAA', 'CJkGEAA', 'CJoGEAA', 'CJsGEAA', 'CJwGEAA', 'CJ0GEAA', 'CJ4GEAA', 'CJ8GEAA', 'CKAGEAA', 'CKEGEAA', 'CKIGEAA', 'CKMGEAA', 'CKQGEAA', 'CKUGEAA', 'CKYGEAA', 'CKcGEAA', 'CKgGEAA', 'CKkGEAA', 'CKoGEAA', 'CKsGEAA', 'CKwGEAA', 'CK0GEAA', 'CK4GEAA', 'CK8GEAA', 'CLAGEAA', 'CLEGEAA', 'CLIGEAA', 'CLMGEAA', 'CLQGEAA', 'CLUGEAA', 'CLYGEAA', 'CLcGEAA', 'CLgGEAA', 'CLkGEAA', 'CLoGEAA', 'CLsGEAA', 'CLwGEAA', 'CL0GEAA', 'CL4GEAA', 'CL8GEAA', 'CMAGEAA', 'CMEGEAA', 'CMIGEAA', 'CMMGEAA', 'CMQGEAA', 'CMUGEAA', 'CMYGEAA', 'CMcGEAA', 'CMgGEAA', 'CMkGEAA', 'CMoGEAA', 'CMsGEAA', 'CMwGEAA', 'CM0GEAA', 'CM4GEAA', 'CM8GEAA', 'CNAGEAA', 'CNEGEAA', 'CNIGEAA', 'CNMGEAA', 'CNQGEAA', 'CNUGEAA', 'CNYGEAA', 'CNcGEAA', 'CNgGEAA', 'CNkGEAA', 'CNoGEAA', 'CNsGEAA', 'CNwGEAA', 'CN0GEAA', 'CN4GEAA', 'CN8GEAA', 'COAGEAA', 'COEGEAA', 'COIGEAA', 'COMGEAA', 'COQGEAA', 'COUGEAA', 'COYGEAA', 'COcGEAA', 'COgGEAA', 'COkGEAA', 'COoGEAA', 'COsGEAA', 'COwGEAA', 'CO0GEAA', 'CO4GEAA', 'CO8GEAA', 'CPAGEAA', 'CPEGEAA', 'CPIGEAA', 'CPMGEAA', 'CPQGEAA', 'CPUGEAA', 'CPYGEAA', 'CPcGEAA', 'CPgGEAA', 'CPkGEAA', 'CPoGEAA', 'CPsGEAA', 'CPwGEAA', 'CP0GEAA', 'CP4GEAA', 'CP8GEAA', 'CIAHEAA', 'CIEHEAA', 'CIIHEAA', 'CIMHEAA', 'CIQHEAA', 'CIUHEAA', 'CIYHEAA', 'CIcHEAA', 'CIgHEAA', 'CIkHEAA', 'CIoHEAA', 'CIsHEAA', 'CIwHEAA', 'CI0HEAA', 'CI4HEAA', 'CI8HEAA', 'CJAHEAA', 'CJEHEAA', 'CJIHEAA', 'CJMHEAA', 'CJQHEAA', 'CJUHEAA', 'CJYHEAA', 'CJcHEAA', 'CJgHEAA', 'CJkHEAA', 'CJoHEAA', 'CJsHEAA', 'CJwHEAA', 'CJ0HEAA', 'CJ4HEAA', 'CJ8HEAA', 'CKAHEAA', 'CKEHEAA', 'CKIHEAA', 'CKMHEAA', 'CKQHEAA', 'CKUHEAA', 'CKYHEAA', 'CKcHEAA', 'CKgHEAA', 'CKkHEAA', 'CKoHEAA', 'CKsHEAA', 'CKwHEAA', 'CK0HEAA', 'CK4HEAA', 'CK8HEAA', 'CLAHEAA', 'CLEHEAA', 'CLIHEAA', 'CLMHEAA', 'CLQHEAA', 'CLUHEAA', 'CLYHEAA', 'LcHEAA', 'CLgHEAA', 'CLkHEAA', 'CLoHEAA', 'CLsHEAA', 'CLwHEAA', 'CL0HEAA', 'CL4HEAA', 'CL8HEAA', 'CMAHEAA', 'CMEHEAA', 'CMIHEAA', 'CMMHEAA', 'CMQHEAA', 'CMUHEAA', 'CMYHEAA', 'CMcHEAA', 'CMgHEAA', 'CMkHEAA', 'CMoHEAA', 'CMsHEAA', 'CMwHEAA', 'CM0HEAA', 'CM4HEAA', 'CM8HEAA', 'CNAHEAA', 'CNEHEAA', 'CNIHEAA', 'CNMHEAA', 'CNQHEAA', 'CNUHEAA', 'CNYHEAA', 'CNcHEAA', 'CNgHEAA', 'CNkHEAA', 'CNoHEAA', 'CNsHEAA', 'CNwHEAA', 'CN0HEAA', 'CN4HEAA', 'CN8HEAA', 'COAHEAA', 'COEHEAA', 'COIHEAA', 'COMHEAA', 'COQHEAA', 'COUHEAA', 'COYHEAA', 'COcHEAA'
        );
        return $tokens[$page - 1];
    }

    function getVideos($args)
    {
        $user = $this->users->getCurrent();
        $channels = array();
        $videos = array();
        foreach ($user['settings']['content']['channels'] as $channel)
        {
            if ($channel['settings']['state'] !== 'on') continue;
            if ($channel['settings']['priority'] === 0) continue;
            $channels[] = $channel;
        }
		shuffle($channels);

        $prioritySum = 0;
        foreach ($channels as $channel)
        {
            $prioritySum += $channel['settings']['priority'];
        }
        if ($prioritySum === 0) return $videos;

        $percentPerScore = 100 / $prioritySum;
        foreach ($channels as $channel)
        {
            $count = ceil($args['count'] / 100 * ($channel['settings']['priority'] * $percentPerScore));
            $videos = array_merge($videos, $this->getRandomVideosFromChannel($channel, $count));
			
			//For cases when channels quantity is more than the requested quantity
			if (sizeof($videos) >= $args['count']) break;
        }
        shuffle($videos);
        return array_slice($videos, 0, $args['count']);
    }

}
