<?php

class Users extends CI_Model
{
    private $user = false;

    function __construct()
    {
        $userId = $this->session->userdata('userId');
        if (!empty($userId)) $this->user = $this->getById($userId);
        if ($this->user === false && in_array(uri_string(), array('settings/content', 'settings', 'video')))
        {
            $add = $this->add();
            $this->session->set_userdata('userId', $add['id']);
            $this->user = $this->getById($add['id']);
        }
    }

    function prepareRow($row)
    {
        if (sizeof($row) === 0) return false;
        $row['settings'] = json_decode($row['settings'], true);
        if ($row['settings'] === null) $row['settings'] = array();
        $defaults = array(
            'general' => array(
                'password' => '',
            ),
            'content' => array(
                'channels' => array(),
            ),
        );
        $row['settings'] = array_replace_recursive($defaults, $row['settings']);
        return $row;
    }

    function getCurrent()
    {
        return $this->user;
    }

    function getById($id)
    {
        $query = $this->db->query(
            "SELECT * FROM users WHERE id = ?",
            array($id)
        );
        return $this->prepareRow((array)$query->row());
    }

    function loginByKey($key)
    {
        $query = $this->db->query(
            "SELECT * FROM users WHERE login_key = ?",
            array($key)
        );
        $this->user = $this->prepareRow((array)$query->row());
        if ($this->user === false) return false;

        $this->session->set_userdata('userId', $this->user['id']);
        return true;
    }

    function add($input = array())
    {
        $dateNow = date('Y-m-d H:i:s', time());
        $ip = getIp();
		if (empty($input['settings']))
		{
			$input['settings'] = include(FCPATH . "/application/config/default_user_options/" . getLangCode() . ".php");
		}
        $this->db->query(
            "INSERT into users SET register_date = ?, settings = ?, login_key = ?, ip = ?",
            array(
                $dateNow,
				json_encode($input['settings']),
                md5("{$dateNow}asvnqoinqe%@GW{$ip}"),
                $ip
            )
        );
        return array('state' => true, 'id' => $this->db->insert_id());
    }


    function update($input)
    {
        $query = $this->db->query(
            "UPDATE users SET settings = ? " .
            "WHERE id = ? LIMIT 1",
            array(
                json_encode($input['settings']),
                $input['id']
            )
        );
        return $query;
    }

}
