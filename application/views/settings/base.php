<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link <?php echo getNavItemClass('settings', true) ?>" data-toggle="tab"
                   href="<?php echo base_url('settings') ?>" role="tab"
                   aria-controls="home" aria-selected="true"><?php _e('settings_general_tab_title') ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?php echo getNavItemClass('settings/content', true) ?>"
                   data-toggle="tab" href="<?php echo base_url('settings/content') ?>" role="tab"
                   aria-controls="profile" aria-selected="false"><?php _e('settings_content_tab_title') ?></a>
            </li>
        </ul>
        <div class="tab-content">
            <?php echo $content ?>
            <div class="alert alert-danger ykSettings__alert ykSettings__alert--danger"
                 role="alert"></div>
            <div class="alert alert-success ykSettings__alert ykSettings__alert--success"
                 role="alert"></div>
            <div class="clearfix">
                <button class="btn btn-success float-right ykSettings__btnSave" data-toggle="button"
                        aria-pressed="false" autocomplete="off">
                    <?php _e('settings_btn_save') ?>
                </button>
            </div>
        </div>
    </div>
</div>