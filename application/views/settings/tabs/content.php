<div class="tab-pane active mb-3" role="tabpanel">
    <h5><?php _e('settings_content_channels_section_title') ?></h5>
    <div id="ykSettingsChannels" class="ykSettingsChannels">
        <div class="ykSettingsChannels__list"></div>
        <div class="ykSettingsChannels__control">
            <div class="form-row">
                <div class="col-md-10">
                    <input type="text" class="form-control ykSettingsChannel__controlInputUrl"
                           placeholder="<?php _e('settings_content_channels_field_channel_link_placeholder') ?>">
					<small class="form-text text-muted">
						<?php _e('settings_content_channels_supported_formats') ?>:<br>
						<b>https://www.youtube.com/@{handle}</b><br>
						<b>https://www.youtube.com/channel/{channel_id}</b><br>
						<b>https://www.youtube.com/user/{user_id}</b><br>
						<b>{channel_id}</b><br>
					</small>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary btn-block ykSettingsChannel__controlBtnAdd" data-toggle="button"
                            aria-pressed="false" autocomplete="off">
                        <?php _e('settings_content_channels_btn_add_channel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
