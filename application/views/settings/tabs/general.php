<div class="tab-pane active ykSettingsGeneral__tab" role="tabpanel">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="secretUrl"><?php _e('settings_general_field_your_url_label') ?></label>
                <input type="text" class="form-control ykSettingsGeneral__inputSecretUrl" id="secretUrl" disabled
                       value="<?php echo htmlspecialchars($secretUrl) ?>">
                <small class="form-text text-muted">
                    <?php
                    echo sprintf(
                            __('settings_general_field_your_url_description'),
                        '<a href="#" class="ykSettingsGeneral__btnCopySecretUrl">'.__('here').'</a>'
                    )
                    ?>
                </small>
            </div>
        </div>
        <!--        <div class="col-md-6">-->
        <!--            <div class="form-group">-->
        <!--                <label for="password">Пароль</label>-->
        <!--                <input type="text" class="form-control" id="password" name="password" disabled maxlength="255"-->
        <!--                       value="--><?php //echo htmlspecialchars($general['password']) ?><!--">-->
        <!--                <small class="form-text text-muted">-->
        <!--                    Этот пароль поможет защитить страницу настроек от доступа ребенком.-->
        <!--                    Оставьте пустым, если пароль не нужен.-->
        <!--                    (функция будет доступна немного позже)-->
        <!--                </small>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--        <div class="col-md-6">-->
        <!--            <div class="form-group">-->
        <!--                <label for="dailyLimit">Ежедневный лимит (часы)</label>-->
        <!--                <input type="text" class="form-control" id="dailyLimit" disabled value="0">-->
        <!--                <small class="form-text text-muted">-->
        <!--                    Вы можете ограничть время показа видео с помощью этой функции. <br>-->
        <!--                    (функция будет доступна немного позже)-->
        <!--                </small>-->
        <!--            </div>-->
        <!--        </div>-->
    </div>
</div>
