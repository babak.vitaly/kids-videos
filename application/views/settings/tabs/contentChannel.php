<div class="card ykSettingsChannel mb-1">
    <div class="card-header ykSettingsChannel__header clearfix" id="headingOne">
        <div class="ykSettingsChannel__headerTitle"></div>
        <div class="ykSettingsChannel__headerControl">
            <div class="ykSettingsChannel__headerBtnRemoveContainer">
                <i class="fas fa-trash-alt collapsed ykSettingsChannel__headerBtnRemove"
                   aria-expanded="false" title="<?php _e('settings_content_channels_btn_remove_channel') ?>"></i>
            </div>
            <div class="ykSettingsChannel__headerBtnSettingsContainer">
                <i class="fas fa-cogs collapsed ykSettingsChannel__headerBtnSettings" data-toggle="collapse"
                   data-target="#ykSettingsChannel_{id}"
                   aria-expanded="false" aria-controls="ykSettingsChannel_{id}"
                   title="<?php _e('settings_content_channels_btn_settings') ?>"></i>
            </div>
            <input class="ykSettingsChannel__headerCheckboxState" type="checkbox" data-yk-field-type="checkbox"
                   title="<?php _e('settings_content_channels_checkbox_state') ?>" name="state">
        </div>
    </div>
    <div id="ykSettingsChannel_{id}" class="collapse p-3 ykSettingsChannel__body"
         data-parent="#ykSettingsChannels">
        <div class="row">
            <div class="form-group col-md-6">
                <label for="ykSettingsChannel{id}__include"><?php _e('settings_content_channels_field_search_label') ?></label>
                <input type="text" class="form-control" id="ykSettingsChannel{id}__include"
                       data-yk-field-type="input" name="include" maxlength="255">
            </div>
            <?php
            //Exclude works only if there is include :| ...
            //            <div class="form-group col-md-6">
            //                <label for="ykSettingsChannel{id}__exclude">Исключить</label>
            //                <input type="text" class="form-control" id="ykSettingsChannel{id}__exclude"
            //                       data-yk-field-type="input" name="exclude" maxlength="255">
            //                <small class="form-text text-muted">
            //                    Исключить те видео, которые содержат этот текст в заголовке
            //                </small>
            //            </div>
            ?>
            <div class="form-group col-md-6">
                <label for="ykSettingsChannel{id}__range"><?php _e('settings_content_channels_field_priority_label') ?></label>
                <input type="range" class="custom-range" min="0" max="10" step="1" data-yk-field-type="input"
                       id="ykSettingsChannel{id}__range" name="priority">
                <small class="form-text text-muted">
                    <?php _e('settings_content_channels_field_priority_description') ?>
                </small>
            </div>
        </div>
    </div>
</div>
