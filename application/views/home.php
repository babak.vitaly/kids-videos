<div class="row">
    <div class="col-md-12">
        <h1>Kids Videos - parent control for YouTube</h1>
        <h6>
            Focus your child only on helpful videos
        </h6>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p>
            While being a father to a toddler I realize how it's important to safeguard our children from a negative
            impact of explicit or just awkward online content. I have spent hours and hours trying to find the right
            parent-control software, and though I managed to find a few phone apps they were full of ads and there were
            no desktop apps whatsoever. So I came up with my own solution!
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h4>Advantages</h4>
        <ul>
            <li>free;</li>
            <li>registration not required;</li>
            <li>ad-free forever;</li>
            <li>works on any device with a browser;</li>
        </ul>
        <h4>How to use?</h4>
        <ol>
            <li>
                Add your Youtube Channels on the
                <a href="<?php echo base_url('settings/content') ?>" target="_blank">"Settings"</a> page.
            </li>
            <li>
                Watch the preselected content on the
                <a href="<?php echo base_url('video') ?>" target="_blank">"Video"</a> page.
            </li>
        </ol>
        <h4>Features to be added</h4>
        <ul>
            <li>looping;</li>
            <li>video quality options;</li>
            <li>password protected access to settings;</li>
            <li>custom search;</li>
            <li>watch time limit;</li>
            <li>channel import from Youtube account;</li>
            <li>playback statistics;</li>
        </ul>
        <p>
            Feedback is welcome and appreciated <a href="mailto:babak.vitaly@gmail.com">babak.vitaly@gmail.com</a>
        </p>
    </div>
</div>
