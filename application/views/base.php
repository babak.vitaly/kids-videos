<!doctype html>
<html lang="<?php echo getLangCode() ?>">
<head>
    <?php if ($GLOBALS['server'] === 'prod'): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131123505-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag()
            {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-131123505-1');
        </script>
    <?php endif; ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title><?php echo $title ?></title>
    <?php $this->base->printStyles() ?>
</head>
<body data-page-name="<?php echo $this->base->getPageName() ?>">
<header class="ykHeader bg-white border-bottom mb-3 mb-sm-4">
    <nav class="navbar navbar-expand-lg navbar-light ykNavigation">
        <a class="navbar-brand" href="<?php echo base_url() ?>">
            <h5 class="my-0 mr-md-auto font-weight-normal">Kids Videos</h5>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#menuMain" aria-controls="menuMain"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="menuMain">
            <ul class="navbar-nav">
                <li class="nav-item <?php echo getNavItemClass('') ?>">
                    <a class="nav-link" href="<?php echo base_url() ?>">
                        <?php _e('menu_main') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo getNavItemClass('video') ?>">
                    <a class="nav-link " href="<?php echo base_url('video') ?>">
                        <?php _e('menu_video') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo getNavItemClass('settings') ?>">
                    <a class="nav-link " href="<?php echo base_url('settings') ?>">
                        <?php _e('menu_settings') ?>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>
<main role="main" class="ykMain container bg-white <?php echo $this->base->getMainClass() ?>">
    <?php echo $content ?>
</main>
<footer class="ykFooter"></footer>
<?php $this->base->printScripts() ?>
<?php $this->base->printPhpVars() ?>
</body>
</html>