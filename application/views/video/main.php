<div class="row">
    <div class="col-lg-9 mb-3 mb-lg-0 ykVideosPlayer">
		<div class="ykVideosPlayer__screen">
			<div class="embed-responsive embed-responsive-16by9">
				<div class="embed-responsive-item" id="ykYoutubePlayer"></div>
			</div>
			<div class="ykVideosPlayer_overlay ykVideosPlayer_overlay--main"></div>
			<div class="ykVideosPlayer_overlay ykVideosPlayer_overlay--logo"></div>
			<div class="ykVideosPlayer_overlay ykVideosPlayer_overlay--fullScreen"></div>
		</div>
        <h6 class="ykVideosPlayer__title"></h6>
        <div class="ykVideosPlayer__control"></div>
    </div>
    <div class="col-lg-3">
        <div class="ykVideosList row">

        </div>
    </div>
</div>
