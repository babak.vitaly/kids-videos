<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cache
{
    private $fileName, $parameters, $callbackGetNewData, $cacheLifetime;

    function __construct($d = NULL)
    {
        $defaults = array(
            'parameters' => array(),
            'cacheLifetime' => 60 * 60 * 24 * 7,
        );
        $d = array_replace_recursive($defaults, $d);
        $this->parameters = $d['parameters'];
        $filename = md5(serialize($this->parameters));
		$d['directory'] = md5($d['directory']);
        $basePath = FCPATH . "/application/cache/youtube/{$d['directory']}";
        if (!is_dir($basePath)) mkdir($basePath, 0777, true);

        $this->fileName = "$basePath/$filename.json";
        //Google_Service_YouTube_Resource_Channels
        $this->callbackGetNewData = $d['callbackGetNewData'];
        $this->cacheLifetime = $d['cacheLifetime'];
    }

    function getNewData()
    {
        return call_user_func_array($this->callbackGetNewData, array($this->parameters));
    }

    function getData()
    {
        $resultData = array();
        $loadedFromCache = true;
        $lastUpdateDate = 0;
        $fileExists = file_exists($this->fileName);
        if ($fileExists)
        {
            $cache = json_decode(file_get_contents($this->fileName), true);
            $resultData = $cache['data'];
            $lastUpdateDate = $cache['updateDate'];
        }
        if (($lastUpdateDate + $this->cacheLifetime) < time())
        {
            $newData = $this->getNewData();
            if (!$newData['state'])
            {
                if ($fileExists)
                {
                    return array(
                        'state' => true,
                        'data' => $resultData,
                        'loadedFromCache' => $loadedFromCache,
                        'failedToLoadNewData' => true,
                    );
                }
                return array('state' => false, 'message' => __('Failed to load a new data.', 'cprty'));
            }
            $this->updateData($newData['data']);
            $resultData = $newData['data'];
            $loadedFromCache = false;
        }
        return array(
            'state' => true,
            'data' => $resultData,
            'loadedFromCache' => $loadedFromCache
        );
    }

    private function updateData($data)
    {
        $content = json_encode(array('data' => $data, 'updateDate' => time()), JSON_PRETTY_PRINT);
        file_put_contents($this->fileName, $content);
    }
}
