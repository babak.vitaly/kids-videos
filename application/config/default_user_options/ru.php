<?php

return array(
	'general' =>
		array(
			'password' => '',
		),
	'content' =>
		array(
			'channels' =>
				array(
					array(
						'id' => 'UCLsooMJoIpl_7ux2jvdPB-Q',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '2',
							),
					),
					array(
						'id' => 'UCAC2gMyB4R2hCBTU3nitSCg',
						'settings' =>
							array(
								'state' => '',
								'include' => '&quot;СИНИЙ ТРАКТОР&quot; -&quot;ПОИГРАЙКА&quot;',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UCs_uv3QyUIQjBoL1Ij5BdlQ',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UCly0Lv5K3bGKF1b_EAB2umg',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UClZkHt2kNIgyrTTPnSQV3SA',
						'settings' =>
							array(
								'state' => '',
								'include' => '&quot;Малышарики&quot; -&quot;Ам Ням&quot;',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UCQD_yZCS2BGO_BXtYrXFg1Q',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UCSLZXHSv1aQDNeLN3GX6bew',
						'settings' =>
							array(
								'state' => '',
								'include' => '&quot;щенячий патруль&quot;|&quot;расти-механик&quot;|&quot;Вспыш и чудо-машинки&quot;|&quot;Умизуми&quot;',
								'priority' => '2',
							),
					),
					array(
						'id' => 'UCbCmjCuTUZos6Inko4u57UQ',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UCoz6Pnk2--MAfDkTvGt5-dw',
						'settings' =>
							array(
								'state' => '',
								'include' => 'Книга Джунглей',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UCD_UH-lQu-5Upx0gZjhji_Q',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '1',
							),
					),
					array(
						'id' => 'UCIOwKErxRgAWecQrVVax2MA',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UCrlFHstLFNA_HsIV7AveNzA',
						'settings' =>
							array(
								'state' => '',
								'include' => '&quot;Простоквашино Все серии подряд&quot;|&quot;Обезьянки мультфильм — все серии подряд&quot;|&quot;Винни Пух — Все серии подряд&quot;|&quot;Как львенок и черепаха пели песню&quot;',
								'priority' => '0',
							),
					),
					array(
						'id' => 'UCQYY7FZ_Xph6HaYZtKaE-HA',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '&quot;Английский язык для детей: Мяу-Мяу&quot;',
								'priority' => '1',
							),
					),
					array(
						'id' => 'UCwICyav2DD_1NZqIvf6F7Jg',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '6',
							),
					),
					array(
						'id' => 'UCVF9-aelWI3oGRtmIKZueaA',
						'settings' =>
							array(
								'state' => '',
								'include' => '&quot;Английский язык для детей&quot;',
								'priority' => '5',
							),
					),
					array(
						'id' => 'UCpOrOzBNdHhBiAlbC1WsPYw',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UC2el0G8cIcOOdjlHy2KOBkQ',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UCw3vK8lNe5SZzL--rMgq-CQ',
						'settings' =>
							array(
								'state' => '',
								'include' => 'Черепашки-ниндзя',
								'priority' => '5',
							),
					),
					array(
						'id' => 'UC-xvVlscVQJu2FpMEBcTMmw',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '5',
							),
					),
					array(
						'id' => 'UCeINP90ZgSq4Fra9DuGtY0g',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '5',
							),
					),
					array(
						'id' => 'UCxM98s7c7R1YTuPNBtItwmA',
						'settings' =>
							array(
								'state' => '',
								'include' => '',
								'priority' => '5',
							),
					),
					array(
						'id' => 'UCiR_DVo6Zf7Pye92bPLTOCQ',
						'settings' =>
							array(
								'state' => 'on',
								'include' => 'Развлечёба',
								'priority' => '8',
							),
					),
				))
);
