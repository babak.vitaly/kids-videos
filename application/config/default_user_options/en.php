<?

return array(
	'general' =>
		array(
			'password' => '',
		),
	'content' =>
		array(
			'channels' =>
				array(
					array(
						'id' => 'UCLsooMJoIpl_7ux2jvdPB-Q',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '10',
							),
					),
					array(
						'id' => 'UCQD_yZCS2BGO_BXtYrXFg1Q',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '3',
							),
					),
					array(
						'id' => 'UCbCmjCuTUZos6Inko4u57UQ',
						'settings' =>
							array(
								'state' => 'on',
								'include' => '',
								'priority' => '5',
							),
					),
				)
		)
);
