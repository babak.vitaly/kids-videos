<?php

//General
$lang = array(
    'here' => 'здесь',
);

//Menu
$lang += array(
    'menu_main' => 'Главная',
    'menu_video' => 'Видео',
    'menu_settings' => 'Настройки',
);

//Home
$lang += array(
    'home_title' => 'Kids Videos - родительский контроль для YouTube',
);

//Video
$lang += array(
    'video_title' => 'Видео',
    'video_error_cant_get_video' => 'Не могу получить видео.',
    'video_notice_there_is_not_video_to_play' => 'Нет видео для воспроизведения. Возможно Вы не добавили каналы. Вы можете добавить каналы на странице настроек.',
);

//Settings
$lang += array(
    'settings_messages_saved' => 'Ваши настройки сохранены.',
    'settings_btn_save' => 'Сохранить',
);

//Settings->General
$lang += array(
    'settings_general_title' => 'Настройки - Основные',
    'settings_general_tab_title' => 'Основные',
    'settings_general_field_your_url_label' => 'Ваша уникальная ссылка',
    'settings_general_field_your_url_description' => 'По этой ссылке вы можете получить доступ к своему аккаунту с других устройств. Нажмите %s чтобы скопировать ее.',
    'settings_general_messages_url_copied' => 'Ваша уникальная ссылка скопированная в буфер обмена.',
);

//Settings->Content
$lang += array(
    'settings_content_title' => 'Настройки - Контент',
    'settings_content_tab_title' => 'Контент',
    'settings_content_channels_field_channel_link_placeholder' => 'Ссылка на канал',
    'settings_content_channels_field_channel_link_validation_incorrect' => 'Ваша ссылка на какнал некорректна.',
    'settings_content_channels_field_channel_link_validation_not_found' => 'Не могу найти канал.',
    'settings_content_channels_btn_add_channel' => 'Добавить',
    'settings_content_channels_section_title' => 'Каналы',
    'settings_content_channels_btn_remove_channel' => 'Удалить канал из списка',
    'settings_content_channels_btn_settings' => 'Настройки для этого канала',
    'settings_content_channels_checkbox_state' => 'Показывать\скрывать видео с этого канала',
    'settings_content_channels_field_search_label' => 'Поиск',
    'settings_content_channels_field_priority_label' => 'Приоритет',
    'settings_content_channels_field_priority_description' => 'Чем выше - чем чаща видео с этого канала будут появлятся в списке воспроизведения',
	'settings_content_channels_supported_formats' => 'Доступные форматы',
);
