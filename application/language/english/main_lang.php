<?php

//General
$lang = array(
    'here' => 'here',
);

//Menu
$lang += array(
    'menu_main' => 'Home',
    'menu_video' => 'Video',
    'menu_settings' => 'Settings',
);

//Home
$lang += array(
    'home_title' => 'Kids Videos - parent control for YouTube',
);

//Video
$lang += array(
    'video_title' => 'Video',
    'video_error_cant_get_video' => 'Can\'t get the video.',
    'video_notice_there_is_not_video_to_play' => 'There is not a video to play. Probably you haven\'t added channels. You can add the channels on the Settings page.',
);

//Settings
$lang += array(
    'settings_messages_saved' => 'Your settings saved.',
    'settings_btn_save' => 'Save',
);

//Settings->General
$lang += array(
    'settings_general_title' => 'Settings - General',
    'settings_general_tab_title' => 'General',
    'settings_general_field_your_url_label' => 'Your unique URL',
    'settings_general_field_your_url_description' => 'By this link, you can use your account on other devices. Click %s to copy the link.',
    'settings_general_messages_url_copied' => 'Your unique URL copied to the clipboard.',
);

//Settings->Content
$lang += array(
    'settings_content_title' => 'Settings - Content',
    'settings_content_tab_title' => 'Content',
    'settings_content_channels_field_channel_link_placeholder' => 'Link to the channel',
    'settings_content_channels_field_channel_link_validation_incorrect' => 'Your link to the channel is incorrect.',
    'settings_content_channels_field_channel_link_validation_not_found' => 'The channel is not found.',
    'settings_content_channels_btn_add_channel' => 'Add',
    'settings_content_channels_section_title' => 'Channels',
    'settings_content_channels_btn_remove_channel' => 'Remove the channel from the list',
    'settings_content_channels_btn_settings' => 'The settings of this channel',
    'settings_content_channels_checkbox_state' => 'Show\hide videos from this channel',
    'settings_content_channels_field_search_label' => 'Search',
    'settings_content_channels_field_priority_label' => 'Priority',
    'settings_content_channels_field_priority_description' => 'The higher the priority the often you will see videos from this channel in your list',
	'settings_content_channels_supported_formats' => 'Supported formats',
);
