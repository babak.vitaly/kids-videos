<?php

function getNavItemClass($url, $exec = false)
{
    if ($exec) return $url === uri_string() ? 'active' : '';

    $url = str_replace('/', '\/', $url);
    return preg_match("/^$url(\/.*|)$/", uri_string(), $matches) === 1 ? 'active' : '';
}

function ajaxReturn($data)
{
    die(json_encode($data, JSON_UNESCAPED_UNICODE));
}


function getAjaxData()
{
    $ci =& get_instance();
    if ($ci->users->getCurrent() === false) goto authError;
    if (!isset($_POST['json'])) goto dataError;
    $data = json_decode($_POST['json'], true);
    if ($data === null) goto dataError;
    return $data;

    authError:
    ajaxReturn(array(
        'state' => false,
        'message' =>
            "Ошибка авторизации. " .
            "Для авторизации, вам необходимо перейти на страницу настроек или ввести секретную ссылку."
    ));

    dataError:
    ajaxReturn(array(
        'state' => false,
        'message' => "Ошибка разбора данных."
    ));
}

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else
    {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function redirectBots()
{
    $bot = isset($_SERVER['HTTP_USER_AGENT']) &&
        preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']);
    if ($bot) redirect(base_url(), 'location', 301);
}

function __($key)
{
    $ci =& get_instance();
    return $ci->lang->line($key);
}

function _e($key)
{
    $ci =& get_instance();
    echo $ci->lang->line($key);
}

function getLangCode()
{
    $ci =& get_instance();
    $codes = array(
        'english' => 'en',
        'russian' => 'ru',
    );
    return $codes[$ci->config->item('language')];
}

function objectToArray($data)
{
	if (is_object($data))
	{
		$data = (array)$data;
	}
	if (is_array($data))
	{
		return array_map('objectToArray', $data);
	}
	return $data;
}
