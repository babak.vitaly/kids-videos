# Kids Videos - parent control for YouTube
## Focus your child only on helpful videos
While being a father to a toddler I realize how it's important to safeguard our children from a negative impact of explicit or just awkward online content. I have spent hours and hours trying to find the right parent-control software, and though I managed to find a few phone apps they were full of ads and there were no desktop apps whatsoever. So I came up with my own solution!

You are welcome to use: https://kids-videos.bavi.dev/video
