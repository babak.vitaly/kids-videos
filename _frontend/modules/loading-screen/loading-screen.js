require('./loading-screen.less');

var LoadingScreen = function (args)
{
    var that = this;
    that.args = args;
    that.init();
};

LoadingScreen.prototype = {
    init: function ()
    {
        var that = this;
        var defaults = {
            animation: true,
            style: 'square',
        };
        that.args = $.extend(true, defaults, that.args);
        that.displayedCounter = 0;
        that.currentState = false;
        that.timeout = 300;
        that.$root = $('<div class="ykLoadingScreen"></div>');
        if (that.args.animation)
        {
            that.$animation = $('<div class="ykLoadingScreen__animation"></div>');
            that.$animation.appendTo(that.$root);
        }
        that.$root.attr('data-yk-style', that.args.style);
        if (that.args.$parentContainer === undefined) that.$parentContainer = $('body');
        else that.$parentContainer = that.args.$parentContainer;
        that.$root.appendTo(that.$parentContainer);
    },
    show: function ()
    {
        var that = this;
        that.displayedCounter++;
        if (that.currentState === true) return;
        that.currentState = true;
        that.timerId = setTimeout(function ()
        {
            that.$root.addClass('ykLoadingScreen--active');
        }, that.timeout);
    },
    hide: function ()
    {
        var that = this;
        that.displayedCounter--;
        if (that.displayedCounter > 0) return;
        clearTimeout(that.timerId);
        if (that.currentState === false) return;
        that.$root.removeClass('ykLoadingScreen--active');
        that.currentState = false;
    }
};


module.exports = LoadingScreen;