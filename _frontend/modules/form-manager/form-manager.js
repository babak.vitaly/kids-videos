var utils = require('utils');


var FormManager = function (args)
{
    var that = this;
    that.args = args;
    that.init();
};

FormManager.prototype = {
    init: function ()
    {
        var that = this;

        //Filter only supported fields
        that.$fields = that.args.$form.find('[data-yk-field-type]');


        //Init fields
        // that.$fields.each(function (key)
        // {
        //     var $field = $(this);
        //     var type = $field.data('yk-field-type');
        //
        // });
    },
    setField: function ($field, value)
    {
        var that = this;
        var type = $field.data('yk-field-type');
        switch (type)
        {
            case 'input':
                $field.val(value);
                break;
            case 'dropdown':
                var $dropdownOptions = $field.find('option');
                $dropdownOptions.attr('selected', false);
                if (value != '') $dropdownOptions.filter('[value=' + value + ']').attr('selected', true);
                $dropdownOptions.change();
                break;
            case 'checkbox':
                $field.prop('checked', value === 'on');
                break;
            case 'checkboxSwitch':
                $field.prop('checked', value === 'on');
                $field.change();
                break;
        }
    },
    reset: function ()
    {
        var that = this;
        that.$fields.each(function ()
        {
            var $field = $(this);
            var value = $field.data('yk-default');
            if (value === undefined) value = '';
            that.setField($field, value);
        });
    },
    set: function (data)
    {
        var that = this;
        that.reset();
        var values = utils.objectToValues(data);
        for (var path in values)
        {
            var value = values[path];
            var $field = that.$fields.filter('[name="' + path + '"]');
            //If element is't input (like image selector)
            if ($field.length === 0) $field = that.$fields.filter('[data-yk-name="' + path + '"]');
            that.setField($field, value);
        }
    },
    get: function ()
    {
        var that = this;
        return utils.serializeObject(that.args.$form);
    }
};

module.exports = FormManager;