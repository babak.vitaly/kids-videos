let utils = {};

utils.getAttributes = function ($node)
{
    let attrs = {};
    if ($node[0] === undefined || $node[0].attributes === undefined) return {};

    $.each($node[0].attributes, function (index, attribute)
    {
        attrs[attribute.name] = attribute.value;
    });
    return attrs;
};

utils.serializeObject = function ($root)
{
    if ($root.prop('tagName') !== 'FORM')
    {
        var queryString;
        //If input(s)
        if ($root.eq(0).filter(':input').length !== 0) queryString = $root.serialize();
        else queryString = $root.find(':input').serialize();
        return utils.deparam(queryString);
    }
    var json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push": /^$/,
            "fixed": /^\d+$/,
            "named": /^[a-zA-Z0-9_]+$/
        };

    function build(base, key, value)
    {
        base[key] = value;
        return base;
    }

    function push_counter(key)
    {
        if (push_counters[key] === undefined)
        {
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    }

    $.each($root.serializeArray(), function ()
        {
            // skip invalid keys
            if (!patterns.validate.test(this.name))
            {
                return;
            }
            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while ((k = keys.pop()) !== undefined)
            {
                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if (k.match(patterns.push))
                {
                    merge = build([], push_counter(reverse_key), merge);
                }

                // fixed
                else if (k.match(patterns.fixed))
                {
                    merge = build([], k, merge);
                }

                // named
                else if (k.match(patterns.named))
                {
                    merge = build({}, k, merge);
                }
            }
            json = $.extend(true, json, merge);
        }
    );
    return json;
};

/*
 Convert object to name - value.
 */
utils.objectToValues = function (o)
{
    var result = [];

    function stringify(o, prefix)
    {
        prefix = prefix || '';
        switch (typeof o)
        {
            case 'object':
                if (Array.isArray(o)) result[prefix] = o;


                var output = "";
                for (var k in o)
                {
                    var dl = '', dr = '';
                    if (prefix != "")
                    {
                        dl = '[';
                        dr = ']';
                    }
                    if (o.hasOwnProperty(k)) output += stringify(o[k], prefix + dl + k + dr);
                }
                return output;
            case 'function':
                return "";
            default:
            {
                result[prefix] = o;
            }
        }
    }

    stringify(o);
    return result;
};


utils.deparam = function (params, coerce)
{
    var obj = {},
        coerce_types = {'true': !0, 'false': !1, 'null': null};

    // If params is an empty string or otherwise falsy, return obj.
    if (!params)
    {
        return obj;
    }

    // Iterate over all name=value pairs.
    params.replace(/\+/g, ' ').split('&').forEach(function (v)
    {
        var param = v.split('='),
            key = decodeURIComponent(param[0]),
            val,
            cur = obj,
            i = 0,

            // If key is more complex than 'foo', like 'a[]' or 'a[b][c]', split it
            // into its component parts.
            keys = key.split(']['),
            keys_last = keys.length - 1;

        // If the first keys part contains [ and the last ends with ], then []
        // are correctly balanced.
        if (/\[/.test(keys[0]) && /\]$/.test(keys[keys_last]))
        {
            // Remove the trailing ] from the last keys part.
            keys[keys_last] = keys[keys_last].replace(/\]$/, '');

            // Split first keys part into two parts on the [ and add them back onto
            // the beginning of the keys array.
            keys = keys.shift().split('[').concat(keys);

            keys_last = keys.length - 1;
        } else
        {
            // Basic 'foo' style key.
            keys_last = 0;
        }

        // Are we dealing with a name=value pair, or just a name?
        if (param.length === 2)
        {
            val = decodeURIComponent(param[1]);

            // Coerce values.
            if (coerce)
            {
                val = val && !isNaN(val) && ((+val + '') === val) ? +val        // number
                    : val === 'undefined' ? undefined         // undefined
                        : coerce_types[val] !== undefined ? coerce_types[val] // true, false, null
                            : val;                                                          // string
            }

            if (keys_last)
            {
                // Complex key, build deep object structure based on a few rules:
                // * The 'cur' pointer starts at the object top-level.
                // * [] = array push (n is set to array length), [n] = array if n is
                //   numeric, otherwise object.
                // * If at the last keys part, set the value.
                // * For each keys part, if the current level is undefined create an
                //   object or array based on the type of the next keys part.
                // * Move the 'cur' pointer to the next level.
                // * Rinse & repeat.
                for (; i <= keys_last; i++)
                {
                    key = keys[i] === '' ? cur.length : keys[i];
                    cur = cur[key] = i < keys_last
                        ? cur[key] || (keys[i + 1] && isNaN(keys[i + 1]) ? {} : [])
                        : val;
                }

            } else
            {
                // Simple key, even simpler rules, since only scalars and shallow
                // arrays are allowed.

                if (Object.prototype.toString.call(obj[key]) === '[object Array]')
                {
                    // val is already an array, so push on the next value.
                    obj[key].push(val);

                } else if ({}.hasOwnProperty.call(obj, key))
                {
                    // val isn't an array, but since a second value has been specified,
                    // convert val into an array.
                    obj[key] = [obj[key], val];

                } else
                {
                    // val is a scalar.
                    obj[key] = val;
                }
            }

        } else if (key)
        {
            // No value was defined, so set something meaningful.
            obj[key] = coerce
                ? undefined
                : '';
        }
    });

    return obj;
};

utils.ajaxRequest = function (args)
{
    const loadingScreen = window.yk.loadingScreen;
    loadingScreen.show();
    $.ajax({
            url: args.url,
            type: "POST",
            data: {json: JSON.stringify(args.data)},
            dataType: "json"
        }
    ).done(function (data)
    {
        loadingScreen.hide();
        args.cb(data);
    }).fail(function (jqXHR, textStatus)
    {
        loadingScreen.hide();
        args.cb({state: false, message: "Request failed: " + textStatus});
    });
};


utils.getPageName = function ()
{
    return $('body').data('page-name');
};

utils.copyToClipboard = function (text)
{
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
};

utils.alert = function (message)
{
    alert(message.replace('<p>', '').replace('</p>', ''));
};

utils.isMobile = function ()
{
	let isMobile = false;
	if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||
		/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
	{
		isMobile = true;
	}
	return isMobile;
};

utils.toggleFullScreen = function ()
{
	if (!document.fullscreenElement)
	{
		document.documentElement.requestFullscreen();
	} else
	{
		if (document.exitFullscreen)
		{
			document.exitFullscreen();
		}
	}
}



module.exports = utils;
