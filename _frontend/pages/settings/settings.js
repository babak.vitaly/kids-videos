require('common');
require('./settings.less');
const utils = require('utils');
const General = require('./general.js');
const Content = require('./content.js');

window.yk.init = function ()
{
    let tab;
    const $btnSave = $('.ykSettings__btnSave');
    const $alertDanger = $('.ykSettings__alert--danger');
    const $alertSuccess = $('.ykSettings__alert--success');
    let alertSuccessTimeoutId = 0;
    const strings = window.phpVars.strings;

    if (utils.getPageName() === 'settingsGeneral')
    {
        tab = new General();
    } else
    {
        tab = new Content();
    }

    function showAlertError(message)
    {
        $alertDanger.html(message);
        $alertSuccess.removeClass('ykSettings__alert--active');
        $alertDanger.addClass('ykSettings__alert--active');
    }

    function showAlertSuccess(message)
    {
        $alertSuccess.html(message);
        $alertDanger.removeClass('ykSettings__alert--active');
        $alertSuccess.addClass('ykSettings__alert--active');
        if (alertSuccessTimeoutId) clearTimeout(alertSuccessTimeoutId);
        alertSuccessTimeoutId = setTimeout(function ()
        {
            $alertSuccess.removeClass('ykSettings__alert--active');
        }, 10000);
    }

    $btnSave.click(function ()
    {
        tab.save(function (data)
        {
            if (data.state === false) showAlertError(data.message);
            else showAlertSuccess(strings.settings_messages_saved);
        });
    });
};
