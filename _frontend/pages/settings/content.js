const utils = require('utils');
const FormManager = require('modules/form-manager/form-manager.js');


const General = function ()
{
    const that = this;
    const phpVars = window.phpVars.pageSettingsContent;
    const settings = phpVars.settings;
    const $channels = $('.ykSettingsChannels');
    const $channelsList = $channels.find('.ykSettingsChannels__list');
    const $controlBtnAdd = $channels.find('.ykSettingsChannel__controlBtnAdd');
    const $controlInputUrl = $channels.find('.ykSettingsChannel__controlInputUrl');
    let channelLastId = 0;

    const Channel = function (args)
    {
        const that = this;
        const $root = $(phpVars.templates.channel);
        const $title = $root.find('.ykSettingsChannel__headerTitle');
        const $checkboxState = $root.find('.ykSettingsChannel__headerCheckboxState');
        const $btnSettings = $root.find('.ykSettingsChannel__headerBtnSettings');
        const $body = $root.find('.ykSettingsChannel__body');
        const $btnRemove = $root.find('.ykSettingsChannel__headerBtnRemove');

        $root.data('object', that);
        const formManager = new FormManager({
            $form: $root,
        });
        formManager.set(args.settings);
        $root.find('*').each(function ()
        {
            const $element = $(this);
            const attributes = utils.getAttributes($element);
            for (let name in attributes)
            {
                let attribute = attributes[name];
                if (name.search(/^data.*/gm) !== -1 && name.search(/^data-target.*/gm) === -1) continue;
                if (attribute.search('{id}') === -1) continue;

                $element.attr(name, attribute.replace(new RegExp('{id}', 'g'), channelLastId))
            }
        });
        $title.text(args.title);
        $root.appendTo($channelsList);
        channelLastId++;

        that.getData = function ()
        {
            return {
                id: args.id,
                title: args.title,
                settings: formManager.get()
            };
        };

        $btnRemove.click(function ()
        {
            $root.remove();
        });
    };

    settings.channels.forEach(function (channel)
    {
        new Channel(channel);
    });

    that.save = function (saved)
    {
        const $channels = $channelsList.find('.ykSettingsChannel');
        const data = {
            channels: [],
        };
        $channelsList.find('.ykSettingsChannel').each(function ()
        {
            const $channel = $(this);
            const channel = $channel.data('object');
            data.channels.push(channel.getData())
        });

        utils.ajaxRequest({
            url: window.phpVars.baseUrl + 'settings/ajaxContentSave',
            data: data,
            cb: function (data)
            {
                if (!data.state)
                {
                    saved({
                        state: false,
                        message: data.message
                    });
                    return;
                }
                saved({state: true});
            }
        });
    };

    $controlBtnAdd.click(function ()
    {
        let url = $controlInputUrl.val();
        utils.ajaxRequest({
            url: window.phpVars.baseUrl + 'settings/ajaxContentAddChannel',
            data: {url: url},
            cb: function (data)
            {
                if (!data.state)
                {
                    utils.alert(data.message);
                    return;
                }
                new Channel(data.channel);
                $controlInputUrl.val('');
            }
        });
    });

};

module.exports = General;