const utils = require('utils');
const FormManager = require('modules/form-manager/form-manager.js');


module.exports = function ()
{
    const that = this;
    const $root = $('.ykSettingsGeneral__tab');
    const $btnCopySecretUrl = $root.find('.ykSettingsGeneral__btnCopySecretUrl');
    const $inputSecretUrl = $root.find('.ykSettingsGeneral__inputSecretUrl');
    const strings = window.phpVars.strings;
    const formManager = new FormManager({
        $form: $root,
    });

    $btnCopySecretUrl.click(function ()
    {
        utils.copyToClipboard($inputSecretUrl.val());
        alert(strings.settings_general_messages_url_copied);
    });

    that.save = function (saved)
    {
        utils.ajaxRequest({
            url: window.phpVars.baseUrl + 'settings/ajaxGeneralSave',
            data: formManager.get(),
            cb: function (data)
            {
                if (!data.state)
                {
                    saved({
                        state: false,
                        message: data.message
                    });
                    return;
                }
                saved({state: true});
            }
        });
    };
};