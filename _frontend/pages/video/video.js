require('common');
require('./video.less');
const utils = require('utils');
const List = require('./list.js');
const Player = require('./player.js');

window.yk.init = function ()
{
    let player, list;
    function getVideos(count, cb)
    {
        utils.ajaxRequest({
            url: window.phpVars.baseUrl + 'video/ajaxGetVideos',
            data: {count: count},
            cb: function (data)
            {
                if (data.state === false)
                {
                    alert(data.message);
                }
                cb(data.videos)
            }
        });
    }

    function getVideo(id, cb)
    {
        utils.ajaxRequest({
            url: window.phpVars.baseUrl + 'video/ajaxGetVideo',
            data: {id: id},
            cb: function (data)
            {
                if (data.state === false)
                {
                    alert(data.message);
                }
                data.video.id = {
                    videoId: data.video.id
                };
                cb(data.video);
            }
        });
    }

    function playVideo(video)
    {
        let count = 10;
        if (video === undefined) count = 11;
		else
		{
			player.setVideo(video);
		}
        getVideos(count, function (videos)
        {
            if (video === undefined)
            {
                player.setVideo(videos[0]);
                list.render(videos.slice(1, videos.length));
            }
            else
            {
                list.render(videos);
            }
        });
    }

    window.addEventListener('popstate', function (e, qwe)
    {
        let videoId =  getVideoIdFromUrl();
        if (videoId !== false)
        {
            getVideo(videoId, function (video)
            {
                playVideo(video);
            });
        }
    });

    player = new Player({
        callbacks: {
            ended: function ()
            {
                playVideo();
            }
        }
    });

    list = new List({
        $root: $('.ykVideosList'),
        player: player,
        callbacks: {
            setVideo: function (video)
            {
                playVideo(video);
            }
        }
    });

    function getVideoIdFromUrl()
    {
        let tmp = location.pathname.match(/^\/video\/play\/(\w*)$/);
        if (tmp === null || tmp[1] === undefined) return false;
        return tmp[1];
    }


	window.onYouTubeIframeAPIReady = function ()
	{
		let videoId = getVideoIdFromUrl();
		if (videoId !== false)
		{
			getVideo(videoId, function (video)
			{
				playVideo(video);
			});
		} else
		{
			playVideo();
		}
	}

	if (utils.isMobile()) $('body').addClass('ykMobile');

};
