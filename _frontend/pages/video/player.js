const utils = require('utils');

const Player = function (args)
{
	const that = this;
	const $title = $('.ykVideosPlayer__title');
	const $body = $('body');
	const $overlayMain = $('.ykVideosPlayer_overlay--main');
	const $overlayFullScreen = $('.ykVideosPlayer_overlay--fullScreen');
	const $player = $('#ykYoutubePlayer');
	let player;

	that.setVideo = function (video)
	{
		const title = `${video.snippet.channelTitle}: ${video.snippet.title}`;
		$title.text(title);
		if (location.pathname !== `/video/play/${video.id.videoId}`)
		{
			let url = `${window.phpVars.baseUrl}video/play/${video.id.videoId}`;
			if (location.pathname === '/video') history.replaceState(null, title, url);
			else history.pushState(null, title, url);
		}

		if (!player)
		{
			const playerOptions = {
				height: '390',
				width: '640',
				videoId: video.id.videoId,
				playerVars: {
					'autoplay': 1,
					'autohide': '0',
				},
				events: {
					'onReady': function (event)
					{
						console.log('onReady', event);
						event.target.playVideo();
					},
					'onStateChange': function (event)
					{
						console.log('onStateChange', event.data);
						switch (event.data)
						{
							case YT.PlayerState.ENDED:
								args.callbacks.ended();
								break;
							case YT.PlayerState.PLAYING:
							case YT.PlayerState.PAUSED:
								$body.toggleClass('ykPlayer--paused', event.data === YT.PlayerState.PAUSED);
								break;
						}
					}

				}
			};
			player = new YT.Player($player.get(0), playerOptions);
		} else
		{
			player.loadVideoById({
				videoId: video.id.videoId
			});
			player.playVideo();
		}
	};

	$overlayMain.on('click', function ()
	{
		if (!player) return

		const ps = player.getPlayerState();
		if (ps === YT.PlayerState.PAUSED || ps === YT.PlayerState.UNSTARTED) player.playVideo();
		else if (player.getPlayerState() === YT.PlayerState.PLAYING) player.pauseVideo();
	});

	$overlayFullScreen.on('click', function ()
	{
		utils.toggleFullScreen();
	});
};


module.exports = Player;
