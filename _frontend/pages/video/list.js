require('./list.less');
const utils = require('utils');

const List = function (args)
{
    const that = this;
    const $root = args.$root;
    const player = args.player;
    const phpVars = window.phpVars.pageVideo;

    function Item(video)
    {
        const $item = $(phpVars.templates.listItem);
        const $preview = $item.find('.ykVideosListItem__preview');
        const $title = $item.find('.ykVideosListItem__title');
        $preview.attr('src', video.snippet.thumbnails.medium.url);
        $title.text(video.snippet.title);
        $item.attr('title', video.snippet.title);
        $item.appendTo($root);
        $item.click(function ()
        {
            args.callbacks.setVideo(video);
        });
    }

    that.render = function(videos)
    {
        $root.find('.ykVideosListItem').remove();
        videos.forEach(function (video)
        {
            new Item(video);
        });
    }
};


module.exports = List;