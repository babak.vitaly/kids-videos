require('jquery');
require('bootstrap/js/dist/util');
require('bootstrap/js/dist/collapse');
require('vendor/fontawesome/less/fontawesome.less');
require('vendor/fontawesome/less/regular.less');
require('vendor/fontawesome/less/solid.less');

require('./bootstrap.scss');
require('./common.less');
const LoadingScreen = require('modules/loading-screen/loading-screen.js');
window.yk = {};


$(document).ready(function ()
{
    window.yk.loadingScreen = new LoadingScreen();
    if (typeof window.yk.init === 'function') window.yk.init();
});