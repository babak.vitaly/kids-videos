<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$config = array(
    'rootDir' => dirname(__FILE__),
    'rootUrl' => 'https://babak.intelimpulse.com/youtube_parental_control',
);

require_once $config['rootDir'] . '/vendor/autoload.php';


$config['oauth'] = array(
    'callbackUrl' => "{$config['rootUrl']}/oauth2callback.php",
    'clientSecretPath' => "{$config['rootDir']}/client_secret_841242279541-euoln62b2353gmvamegvpfg8d96hkepa.apps.googleusercontent.com.json",
    'metaData' => Google_Service_YouTube::YOUTUBE_READONLY,
);
